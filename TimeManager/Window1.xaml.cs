﻿/*
 * Created by SharpDevelop.
 * User: alexey.orlov
 * Date: 07.04.2017
 * Time: 11:13
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;

namespace TimeManager
{
	/// <summary>
	/// Interaction logic for Window1.xaml
	/// </summary>
	public partial class Window1 : Window
	{
		DateTime AnfZeitBew1 = new DateTime();
		DateTime AnfZeitBew2 = new DateTime();
		DateTime AnfZeitBew3 = new DateTime();
		DateTime AnfZeitBew4 = new DateTime();
		DateTime AnfZeitBew5 = new DateTime();
		
		DateTime BegVgBew1 = new DateTime();
		DateTime BegVgBew2 = new DateTime();
		DateTime BegVgBew3 = new DateTime();
		DateTime BegVgBew4 = new DateTime();
		DateTime BegVgBew5 = new DateTime();
		
		DateTime EndVgBew1 = new DateTime();
		DateTime EndVgBew2 = new DateTime();
		DateTime EndVgBew3 = new DateTime();
		DateTime EndVgBew4 = new DateTime();
		DateTime EndVgBew5 = new DateTime();
		
		public Window1()
		{
			InitializeComponent();
		}
		
		void AnfZeitBew1_Button_Click(object sender, RoutedEventArgs e)
		{
			AnfZeitBew1 = DateTime.Now;
			AnfZeitBew1_TextBlock.Text = AnfZeitBew1.ToShortTimeString();
		}
		
		void BeginVgBew1_Button_Click(object sender, RoutedEventArgs e)
		{
			BegVgBew1 = DateTime.Now;
			BeginVgBew1_TextBlock.Text = BegVgBew1.ToShortTimeString();
		}
		
		void EndVgBew1_Button_Click(object sender, RoutedEventArgs e)
		{
			EndVgBew1 = DateTime.Now;
			EndVgBew1_TextBlock.Text = EndVgBew1.ToShortTimeString();
			TimeSpan duration = EndVgBew1 - BegVgBew1;
			DauerVgBew1_TextBlock.Text = duration.ToString(@"mm");
			DateTime abgabe = new DateTime();
			abgabe = AnfZeitBew1.AddMinutes(90) + duration;
			AbgabeBew1_TextBlock.Text = abgabe.ToShortTimeString();
		}
		
		void AnfZeitBew2_Button_Click(object sender, RoutedEventArgs e)
		{
			AnfZeitBew2 = DateTime.Now;
			AnfZeitBew2_TextBlock.Text = AnfZeitBew2.ToShortTimeString();
		}
		
		void BeginVgBew2_Button_Click(object sender, RoutedEventArgs e)
		{
			BegVgBew2 = DateTime.Now;
			BeginVgBew2_TextBlock.Text = BegVgBew2.ToShortTimeString();
		}
		
		void EndVgBew2_Button_Click(object sender, RoutedEventArgs e)
		{
			EndVgBew2 = DateTime.Now;
			EndVgBew2_TextBlock.Text = EndVgBew2.ToShortTimeString();
			TimeSpan duration = EndVgBew2 - BegVgBew2;
			DauerVgBew2_TextBlock.Text = duration.ToString(@"mm");
			DateTime abgabe = new DateTime();
			abgabe = AnfZeitBew2.AddMinutes(90) + duration;
			AbgabeBew2_TextBlock.Text = abgabe.ToShortTimeString();
		}
		
		void AnfZeitBew3_Button_Click(object sender, RoutedEventArgs e)
		{
			AnfZeitBew3 = DateTime.Now;
			AnfZeitBew3_TextBlock.Text = AnfZeitBew3.ToShortTimeString();
		}
		
		void BeginVgBew3_Button_Click(object sender, RoutedEventArgs e)
		{
			BegVgBew3 = DateTime.Now;
			BeginVgBew3_TextBlock.Text = BegVgBew3.ToShortTimeString();
		}
		
		void EndVgBew3_Button_Click(object sender, RoutedEventArgs e)
		{
			EndVgBew3 = DateTime.Now;
			EndVgBew3_TextBlock.Text = EndVgBew3.ToShortTimeString();
			TimeSpan duration = EndVgBew3 - BegVgBew3;
			DauerVgBew3_TextBlock.Text = duration.ToString(@"mm");
			DateTime abgabe = new DateTime();
			abgabe = AnfZeitBew3.AddMinutes(90) + duration;
			AbgabeBew3_TextBlock.Text = abgabe.ToShortTimeString();
		}
		
		void AnfZeitBew4_Button_Click(object sender, RoutedEventArgs e)
		{
			AnfZeitBew4 = DateTime.Now;
			AnfZeitBew4_TextBlock.Text = AnfZeitBew4.ToShortTimeString();
		}
		
		void BeginVgBew4_Button_Click(object sender, RoutedEventArgs e)
		{
			BegVgBew4 = DateTime.Now;
			BeginVgBew4_TextBlock.Text = BegVgBew4.ToShortTimeString();
		}
		
		void EndVgBew4_Button_Click(object sender, RoutedEventArgs e)
		{
			EndVgBew4 = DateTime.Now;
			EndVgBew4_TextBlock.Text = EndVgBew4.ToShortTimeString();
			TimeSpan duration = EndVgBew4 - BegVgBew4;
			DauerVgBew4_TextBlock.Text = duration.ToString(@"mm");
			DateTime abgabe = new DateTime();
			abgabe = AnfZeitBew4.AddMinutes(90) + duration;
			AbgabeBew4_TextBlock.Text = abgabe.ToShortTimeString();
		}
		
		void AnfZeitBew5_Button_Click(object sender, RoutedEventArgs e)
		{
			AnfZeitBew5 = DateTime.Now;
			AnfZeitBew5_TextBlock.Text = AnfZeitBew5.ToShortTimeString();
		}
		
		void BeginVgBew5_Button_Click(object sender, RoutedEventArgs e)
		{
			BegVgBew5 = DateTime.Now;
			BeginVgBew5_TextBlock.Text = BegVgBew5.ToShortTimeString();
		}
		
		void EndVgBew5_Button_Click(object sender, RoutedEventArgs e)
		{
			EndVgBew5 = DateTime.Now;
			EndVgBew5_TextBlock.Text = EndVgBew5.ToShortTimeString();
			TimeSpan duration = EndVgBew5 - BegVgBew5;
			DauerVgBew5_TextBlock.Text = duration.ToString(@"mm");
			DateTime abgabe = new DateTime();
			abgabe = AnfZeitBew5.AddMinutes(90) + duration;
			AbgabeBew5_TextBlock.Text = abgabe.ToShortTimeString();
		}
	}
}